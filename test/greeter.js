const Greeter = artifacts.require("Greeter");

contract('Greeter', async (account) => {

    beforeEach(async () => {
        //web3.personal.truff(web3.eth.accounts[0], 'Passw0rd');
    });

    it("Test greet", async() => {
        let instance = await Greeter.deployed();
        const retorno = await instance.greet('');
        assert.notEmpty(retorno);
        console.log(retorno.logs[0].event);
        assert.equal(retorno.logs[0].event, "LogGreeter");
        //assert.equal("Hello, World");
    });

    it("Test other greet", async() => {
        let instance = await Greeter.deployed();
        const retorno = await instance.greet('Hello');
        assert.notEmpty(retorno);
    });

    it ("Test kill", async() => {
        let instance = await Greeter.deployed();
        const retorno = await instance.kill();
        assert.notEmpty(retorno);
    })

})